USE sapegingr;

DROP TABLE IF EXISTS Subnets;

CREATE EXTERNAL TABLE Subnets (
    ip STRING,
    mask STRING
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY  '\t'
STORED AS TEXTFILE
LOCATION '/data/subnets/variant1';

SELECT * from Subnets limit 10;


DROP TABLE IF EXISTS Users;

CREATE EXTERNAL TABLE Users (
    ip STRING, browser STRING, gender STRING, age INT
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.RegexSerDe'
WITH SERDEPROPERTIES (
    "input.regex" = '^(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})+\\s+(\\S+)+\\s+(\\S+)+\\s+(\\d+)'
)
STORED AS TEXTFILE
LOCATION '/data/user_logs/user_data_M';


select * from Users limit 10;

DROP TABLE IF EXISTS IPRegions;

CREATE EXTERNAL TABLE IPRegions (
    ip STRING,
    region STRING
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY  '\t'
STORED AS TEXTFILE
LOCATION '/data/user_logs/ip_data_M';

SELECT * from IPRegions limit 10;

ADD JAR /opt/cloudera/parcels/CDH/lib/hive/lib/hive-serde.jar;


DROP TABLE IF EXISTS Logs1;

CREATE EXTERNAL TABLE Logs1 (
    ip STRING, datetime INT, http STRING, size INT, code INT, info STRING
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.RegexSerDe'
WITH SERDEPROPERTIES (
    "input.regex" = '^((?:\\d{1,3}\\.){3}\\d{1,3})+\\s+(\\d{8})\\d+\\s+(\\S+)+\\s+(\\d+)+\\s+(\\d+)+\\s+(\\S+)\\s+.*$'
)
STORED AS TEXTFILE
LOCATION '/data/user_logs/user_logs_M';

SET hive.query.name="partitioning test";
SET hive.exec.dynamic.partition.mode=nonstrict;
SET hive.exec.max.dynamic.partitions=500;
SET hive.exec.max.dynamic.partitions.pernode=500;

DROP TABLE IF EXISTS Logs;

CREATE EXTERNAL TABLE Logs (
    ip STRING, http STRING, size INT, code INT, info STRING
)
PARTITIONED BY (datetime INT)
STORED AS TEXTFILE;

INSERT OVERWRITE TABLE Logs PARTITION (datetime)
SELECT ip, http, size, code, info, datetime from Logs1;

select * from Logs limit 10;
DROP TABLE IF EXISTS Logs1;

